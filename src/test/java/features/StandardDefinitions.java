package features;

import fr.esgi.domain.city.CityName;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

import java.util.List;

import static fr.esgi.domain.city.CityName.*;
import static fr.esgi.domain.city.CityName.LONDON;
import static org.assertj.core.api.Assertions.assertThat;

public class StandardDefinitions {

    private final TestContext ctx;
    public StandardDefinitions(TestContext ctx) {
        this.ctx = ctx;
    }

    @Given("Occidental sub-network")
    public void occidentalSubNetwork() {
        network().registerCity(PARIS);
        network().registerCity(LONDON);
        network().registerCity(MILAN);
        network().registerCity(ESSEN);

        network().createLink(PARIS, List.of(LONDON, ESSEN));
        network().createLink(LONDON, List.of(PARIS, ESSEN));
        network().createLink(ESSEN, List.of(PARIS, LONDON, MILAN));
        network().createLink(MILAN, List.of(ESSEN));
    }

    private Network network() {
        return ctx.network();
    }

    @Then("{city} is linked to {cities}")
    public void cityIsLinkedToCities(CityName cityName, List<CityName> cityNames) {
        assertThat(network().get(cityName)).containsExactlyInAnyOrderElementsOf(cityNames);
    }

}
