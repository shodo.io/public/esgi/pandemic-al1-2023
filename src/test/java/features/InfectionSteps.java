package features;

import fr.esgi.domain.city.City;
import fr.esgi.domain.city.CityName;
import io.cucumber.java.ParameterType;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static fr.esgi.domain.city.CityName.valueOf;

public class InfectionSteps {
    private final TestContext ctx;

    public InfectionSteps(TestContext ctx) {
        this.ctx = ctx;
    }

    @ParameterType(".*")
    public CityName city(String name) {
        CityName cityName = valueOf(name);
        City city = getNetwork().getOrDefault(cityName, new City(cityName, 0));
        getNetwork().put(cityName, city);
        return cityName;
    }

    private Map<CityName, City> getNetwork() {
        return ctx.network().network();
    }

    @ParameterType(".*")
    public List<CityName> cities(String names) {
        return Arrays.stream(names.split(",")).map(CityName::valueOf).toList();
    }

    @Given("{city} has not been infected by blue")
    public void cityHasNotBeenInfectedByBlue(CityName cityName) {
        getNetwork().put(cityName, new City(cityName, 0));
    }

    @When("{city} is infected by blue")
    public void parisIsInfectedByBlue(CityName cityName) {
        getNetwork().put(cityName, getNetwork().get(cityName).blueInfection());
    }

    @Then("{city} should have {int} blue marker(s)")
    public void parisShouldHaveBlueMarker(CityName cityname, int blueMarker) {
        Assertions.assertThat(getNetwork().get(cityname).blueMarker()).isEqualTo(blueMarker);
    }

    @Given("{city} has been infected once by blue")
    public void cityHasBeenInfectedOnceByBlue(CityName cityName) {
        getNetwork().put(cityName, getNetwork().get(cityName).blueInfection());
    }

    @Given("{city} has been infected {int} times by blue")
    public void parisHasBeenInfectedTimesByBlue(CityName cityName, int blueInfections) {
        for (int i = 0; i < blueInfections; i++) {
            getNetwork().put(cityName, getNetwork().get(cityName).blueInfection());
        }

    }
}
