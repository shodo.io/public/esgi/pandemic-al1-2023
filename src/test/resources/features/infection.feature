Feature: City Infection

  Background:
    Given Occidental sub-network

  Scenario: First Infection
    Given PARIS has not been infected by blue
    When PARIS is infected by blue
    Then PARIS should have 1 blue marker

  Scenario: Second Infection
    Given PARIS has been infected once by blue
    When PARIS is infected by blue
    Then PARIS should have 2 blue markers

  Scenario: Fourth Infection
    Given PARIS has been infected 3 times by blue
    When PARIS is infected by blue
    Then PARIS should have 3 blue markers
