Feature: Standard Definitions
  Scenario: Occidental sub-network definition
    Given Occidental sub-network
    * PARIS is linked to LONDON,ESSEN
    * LONDON is linked to PARIS,ESSEN
    * ESSEN is linked to PARIS,LONDON,MILAN
    * MILAN is linked to ESSEN
