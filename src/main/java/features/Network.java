package features;

import fr.esgi.domain.city.City;
import fr.esgi.domain.city.CityName;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public record Network(Map<CityName, City> network, Map<CityName, List<CityName>> links) {
    public Network() {
        this(new HashMap<>(), new HashMap<>());
    }

    List<CityName> createLink(CityName cityName, List<CityName> linkedCities) {
        return links().put(cityName, linkedCities);
    }

    City registerCity(CityName name) {
        return network().put(name, new City(name, 0));
    }

    public List<CityName> get(CityName cityName) {
        return links().get(cityName);
    }
}