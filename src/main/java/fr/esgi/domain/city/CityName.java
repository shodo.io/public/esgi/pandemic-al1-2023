package fr.esgi.domain.city;

public enum CityName {
    PARIS, MILAN, ESSEN, LONDON
}
