package fr.esgi.domain.city;

public record City(CityName cityName, int blueMarker) {
    private static final int MAX_INFECTION_TIME = 3;

    public City blueInfection(){
        if (blueMarker == MAX_INFECTION_TIME){
            return this;
        }
        return new City(cityName, blueMarker + 1);
    }
}
